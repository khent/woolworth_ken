AppCore.config(['$routeProvider', '$locationProvider', '$httpProvider',
  function($routeProvider, $locationProvider, $httpProvider) {
    //$httpProvider.defaults.withCredentials = true;
    $routeProvider
      .when('/ng-view', {
        templateUrl : 'ng/templates/home/myhome.html'
        ,controller  : 'personController'
      })
      .when('/newcourierbookrequest', {
        templateUrl : 'ng/templates/courierbooks/courierbookingform.html'
        ,controller  : 'newRequestCtrl'
      });
      // .otherwise({
      //   templateUrl : 'ng/templates/mypartial.html'
      //   ,controller  : 'newRequestCtrl'
      // });
      $locationProvider.html5Mode(true);
}]);

