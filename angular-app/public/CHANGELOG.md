### Version 2.0.0 - 2014/11/01
* The code is 100% rewritten.
* Update effects.
* New effects.
* Responsive 100%.
* Native and pure 100% JavaScript.
* Combine and minify CSS and JS.
* Updated demo page.

### Version 1.1.3 - 2014/02/18
* Remove unnecessary code.
* Bug fix on the text to be displayed when there is an error.

### Version 1.1.2 - 2013/12/10
* Bug fix on callback that fires once is closed in the overlay.
* Bug fix IE8 when open the modal.
* Changes in the sample page.

### Version 1.1.1 - 2013/11/05
* Bug fix on the position scrollbar.

### Version 1.1.0 - 2013/10/22
* Added new property of the position to the output.
* Added support responsive.
* Added property hide scrollbar automatically.
* Cleaning stylesheets for the modern browsers.
* Bug fixes.

### Version 1.0.2 - 2013/10/08
* Bug fix on callback that fires once is closed.

### Version 1.0.1 - 2013/10/04
* Bug fix IE10.

### Version 1.0.0 - 2013/10/01
* First release.

### Version 0.1b - 2013/09/06
* First beta release.