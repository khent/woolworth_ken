AppFilters.filter('greet', function() {
    return function(name) {
        return 'Hello, ' + name + '!';
    };
});
AppFilters.filter('spliter', function() {
    return function(input, splitChar, splitIndex) {
        return true;
        //return (splitIndex>=0) ? input.split(splitChar)[splitIndex] : input.split(splitChar);
    };
});

Date.prototype.minusHours = function(h) {
   this.setTime(this.getTime() + (h*60*60*1000));
   return this;
}

Date.prototype.stdTimezoneOffset = function() {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.dst = function() {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

function formatDate(date) {
    var d = new Date(date) //.minusHours(4);
    var hh = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();
    var dd = "AM";
    var h = hh;

    if (h >= 12) {
        h = hh-12;
        dd = "PM";
    }

    // minus toronto timezone
    var today = new Date();
    h = (today.dst()) ? h - 4 : h - 5;

    if (h == 0) {
        h = 12;
    }

    if (h < 0)  {

        dd = (hh > 15) ? "PM" : "AM";
        dd = (hh > -1 && hh < 4) ? "PM" : dd;
        switch(h)   {
            case -4:
                h = 8;
                break
            case -3:
                h = 9;
                break
            case -2:
                h = 10;
                break
            case -1:
                h = 11;
                break
        }
    }

    m = m<10?"0"+m:m;

    s = s<10?"0"+s:s;

    /* if you want 2 digit hours:
    h = h<10?"0"+h:h; */

    var pattern = new RegExp("0?"+hh+":"+m+":"+s);

    var replacement = h+":"+m;
    /* if you want to add seconds
    replacement += ":"+s;  */
    replacement += " "+dd;

    return date.replace(pattern,replacement);
}

AppFilters.filter('format_date', function() {
    return function(datetime) {
        if(datetime && datetime != '---'){

            var dt = new Date(datetime);
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };
            dt = dt.toUTCString("en-us", options).split(' ');
            return formatDate(dt[2] + " " + dt[1] + ', ' + dt[3] + " " + dt[4]);
        }
        else{
            return "----";
        }

    };
});

AppFilters.filter('format_date_only', function() {
    return function(datetime) {
        if(datetime){
            var dt = new Date(datetime);
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric"
            };
            var date = dt.toLocaleTimeString("en-us", options);

            date = date.split(" ");
            var length = date.length;
            date = date[0] + " " + date[1] + " " + date[2] + " " + date[3];
            return date;

        }
        else {
            return "--";
        }

    };
});

AppFilters.filter('format_time_only', function() {
    return function(datetime) {
        if(datetime){
            var dt = new Date(datetime);
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };
            var date = dt.toLocaleTimeString("en-us", options);
            date = date.split(" ");
            var length = date.length;
            date = date[length - 2] + " " + date[length - 1];
            return date;
        }
        else {
            return "--";
        }

    };
});

AppFilters.filter('object2Array', function() {
    return function(input) {
        var out = [];
        for(i in input){
        out.push(input[i]);
        }
        return out;
    }
})

AppFilters.filter('calculate_waiting', function() {
    function nearest(n, v) {
        n = n / v;
        n = (n < 0 ? Math.floor(n) : Math.ceil(n)) * v;
        n = n / 60;
        return n;
    }
    return function(datetime) {

        var current_date = Math.round(new Date().getTime() / 1000);
        total =  current_date - datetime;
        calculated_waiting_time = nearest(total, 60)
        //var calculated_waiting_time = (total < 1) ? total : Math.round(total);
        return calculated_waiting_time;
    };
});

AppFilters.filter('seconds_to_minutes', function() {
    return function(seconds) {

        total = seconds / 60;
        if(total > 0)   {
            return total + ' minutes';
        }   else    {
            return seconds + 'seconds'
        }

    };
});
