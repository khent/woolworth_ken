
var AppControllers = angular.module('app.controllers', ['ngRoute']);
var AppServices = angular.module('app.services', ['ngCookies']);
var AppDirectives = angular.module('app.directives', []);
var AppFilters = angular.module('app.filters', []);
var AppConstants = angular.module('app.constants', []);
var AppCore = angular.module('app.core', ['ngRoute', 'ngCookies']);

var AppUIControllers = angular.module('app.uicontrollers', ['ngRoute','ui.bootstrap']);

var MainAppModule = angular.module('app', [
    'app.services',
    'app.controllers',
    'app.uicontrollers',
    'app.core'
]);

