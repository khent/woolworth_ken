
AppControllers.controller('applicationController', function($scope,$rootScope, $routeParams, $location, $cookieStore, NoteRest){
    $scope.links = {};
    $scope.results = {};

    console.log('applicationController');

    if ($rootScope.globals.currentUser) {
        //console.log('current user: ' + $rootScope.globals.currentUser.username)
        $scope.account = { currentUser: $rootScope.globals.currentUser.username }


        NoteRest.canvasList($scope.account.currentUser , function(response) {
            console.log('AppControllers === canvasList ===');
            //console.log(response);

            if(response.success) {
                $scope.links = response.data;
            } else {
                $scope.errors = response.data;
            }
            $scope.dataLoading = false;
        });


        $scope.getClass = function(path) {

            if(path === '/') {
                if($location.path() === '/') {
                    return "active";
                } else {
                    return "";
                }
            }

            if ($routeParams.canvas_id == path) {
              return "active"
            } else {
              return ""
            }
        }

    }



});
