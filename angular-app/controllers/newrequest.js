AppUIControllers.controller('newRequestCtrl', function ($scope, $rootScope, $routeParams, $cookies, $cookieStore, $http, $location, NewRequestRest, $modal){
    "use strict";
$scope.message = "hello";
$rootScope.storelist = {};
$rootScope.costcentres = {};
$rootScope.user_fullname = '';
$rootScope.selectedstoredata = {};
$scope.showRequestForm = function(){

    $scope.getUserFullName();
    $scope.loadCostCentre();
    $scope.loadStore();
    $location.path("newcourierbookrequest");

};
$scope.pickupstore_key = "";
$scope.destinationstore_key = "";



// create new rest provider that will pass the pick up store key
// and destination store key to back end
// see the logs for refrence         -- khent
$scope.pass_storeKey = function(){
// console.log("pickupstore_key == ", $scope.pickupstore_key);
// console.log("\ndestinationstore_key == ", $scope.destinationstore_key);
$scope.getStoreDetails($scope.pickupstore_key);


};


$scope.getUserFullName = function(){
                NewRequestRest.getUserFullName()
                                .success(function(full_name, status, headers, config){
                                    if(status == 200){
                                                $rootScope.user_fullname = full_name;
                                    }
                                }).error(function(data, status, headers, config){
                                   alert("not okay");

                                });
};
$scope.loadCostCentre = function(){
            NewRequestRest.costcentres()
                                .success(function(ccdata, status, headers, config){
                                    if(status == 200){
                                        $rootScope.costcentres = ccdata;
                                    }
                                }).error(function(data, status, headers, config){
                                   alert("not okay");
                                   $rootScope.costcentres = {};

                                });

};
$scope.loadStore = function(){
            NewRequestRest.storelist()
                                .success(function(storedata, status, headers, config){
                                    if(status == 200){
                                        $rootScope.storelist = storedata;
                                    }
                                }).error(function(data, status, headers, config){
                                   alert("not okay");
                                   $rootScope.storelist = {};
                                });

};

$scope.getStoreDetails = function(number){
     NewRequestRest.getStoreDetails(number)
                                .success(function(selectedstoredata, status, headers, config){
                                    if(status == 200){
                                        $rootScope.selectedstoredata = selectedstoredata;
                                        console.log($rootScope.selectedstoredata);
                                    }
                                }).error(function(data, status, headers, config){
                                   alert("not okay");
                                   $rootScope.selectedstoredata = {};
                                });
};


});


