AppDirectives.directive('loginForm', function($animate) {

  return {
     restrict: "E",
     templateUrl: "ng/templates/login/login.html"
  };
});


AppDirectives.directive('registerForm', function($animate) {

  return {
     restrict: "E",
     templateUrl: "ng/templates/login/register.html"
  };
});


AppDirectives.directive('navBar', function($animate) {

  return {
     restrict: "E",
     templateUrl: "ng/templates/navbar.html"
  };
});

AppDirectives.directive('notesBtn', function($animate) {

  return {
     restrict: "E",
     templateUrl: "ng/templates/notes/notes-btn.html"
  };
});
