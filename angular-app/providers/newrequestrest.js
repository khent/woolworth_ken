AppServices.service('NewRequestRest', function($http, $cookieStore, $rootScope, $timeout, $window) {

return {
      // -----------------------------------------------------------------------
        storelist: function(params){
            return $http.get('/api/courierbooks/storelist');
        },
        costcentres: function(params){
            return $http.get('api/courierbooks/costcentres');
        },
        getUserFullName: function(params){
            return $http.get('api/courierbooks/getUserFullName');
        },
        getStoreDetails: function(number){
            return $http.get('api/courierbooks/getstoredetails/'+number);
        }


    };

});
//rename services to provider
