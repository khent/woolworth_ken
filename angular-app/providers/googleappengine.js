//EdgeBoilerplate GoogleAppEngine REST Service

AppServices.service('GoogleAppEngine', function($cookies, $filter) {
    var cookie = $cookies.dev_appserver_login;

    return {
        cookie: cookie,
        user: $filter('spliter')(cookie, ':', 0),
        admin: $filter('spliter')(cookie, ':', 1),
        sessionid: $filter('spliter')(cookie, ':', 2)
    };
});
